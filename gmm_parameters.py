import argparse
import os
import torch
import numpy as np

from aff_mnist.vae import VAEPredictor, VAEPredictor2
from aff_mnist.train_predictor_vae import get_pred_generator


class GMMParameters:
    def __init__(self, latent_size, dim_h, sd_path, vae_type, dim_intermediate=None, gpu=False):
        if vae_type == 'conv':
            self.mdl = VAEPredictor(latent_size, dim_h, gpu)
        elif vae_type == 'conv2':
            self.mdl = VAEPredictor2(latent_size, dim_h, dim_intermediate, gpu)
        self.mdl.load_state_dict(torch.load(sd_path))
        self.mdl.eval()
        self.gpu = gpu
        if self.gpu:
            self.mdl = self.mdl.cuda()
        self.variances = []
        self.means = []

    def get_parameters(self, data_path, batch_size, binarise, pixel_threshold, digit, objective, save_path=None):

        train_gen = get_pred_generator(data_path, batch_size, binarise, digit, objective, pixel_threshold)

        for train_data, _, _ in train_gen:
            if self.gpu:
                train_data = train_data.cuda()
            enc_mean, enc_logvar = self.mdl.encode(train_data)
            self.means.append(enc_mean.detach().cpu().numpy())
            self.variances.append(torch.exp(enc_logvar).detach().cpu().numpy())

        self.means = np.vstack(self.means)
        self.variances = np.vstack(self.variances)

        if save_path is not None:
            if not os.path.exists(save_path):
                os.mkdir(save_path)
            np.save(os.path.join(save_path, 'means.npy'), self.means)
            np.save(os.path.join(save_path, 'variances.npy'), self.variances)


def load_parameters(save_path):
    means = np.load(os.path.join(save_path, 'means.npy'))
    variances = np.load(os.path.join(save_path, 'variances.npy'))
    return means, variances


parser = argparse.ArgumentParser()
parser.add_argument('--batch-size', type=int, default=512)
parser.add_argument('--data-path', type=str, default='aff_mnist/training_and_validation_batches')
parser.add_argument('--gpu', action='store_true')
parser.add_argument('--sd-path', type=str)
parser.add_argument('--latent-size', type=int, default=20)
parser.add_argument('--dim-h', type=int, default=25)
parser.add_argument('--digit', type=int, default=None)
parser.add_argument('--binarise', action='store_true')
parser.add_argument('--save-path', type=str)
parser.add_argument('--objective', choices=['rotation', 'thickness', 'aspect_ratio'])
parser.add_argument('--vae-type', choices=['conv', 'conv2'])
parser.add_argument('--dim-intermediate', type=int, default=800)
parser.add_argument('--pixel-threshold', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    parameters = GMMParameters(latent_size=args.latent_size, dim_h=args.dim_h, vae_type=args.vae_type,
                               dim_intermediate=args.dim_intermediate, sd_path=args.sd_path, gpu=args.gpu)
    parameters.get_parameters(data_path=args.data_path, batch_size=args.batch_size, binarise=args.binarise,
                              digit=args.digit, save_path=args.save_path, objective=args.objective,
                              pixel_threshold=args.pixel_threshold)
