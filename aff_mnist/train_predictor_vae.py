from argparse import ArgumentParser
import numpy as np
import os
from sklearn.decomposition import PCA
import torch
import torch.optim as optim
from torch.nn import functional
from torchvision.datasets import MNIST
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor

from aff_mnist.vae import VAEPredictor, VAEPredictor2, elbo_loss
from aff_mnist.train import filter_digit

def get_pred_generator(data_path, batch_size, binarise=False, digit=None, objective=None, image_grad_alg=None,
                       pixel_threshold=False):
    dataset = MNISTPredDataset(digit, data_path, download=True, binarise=binarise, objective=objective,
                               image_grad_alg=image_grad_alg, pixel_threshold=pixel_threshold)
    generator = DataLoader(dataset, batch_size)
    return generator

def get_coeffs(img, algorithm, pc=1):
    if algorithm == 'least_squares':
        A_list = []
        b = []
        for col_num, img_col in enumerate(img.T):
            for row_num, pixel in enumerate(img_col):
                A_list.append([1*np.sqrt(pixel), col_num*np.sqrt(pixel)])
                b.append((39-row_num) * np.sqrt(pixel))
        A = np.array(A_list)
        b = np.array(b)
        coeffs, _, _, _ = np.linalg.lstsq(A, b)
    elif algorithm == 'PCA':
        pca = PCA()
        pca.fit(np.array(np.where(img == 1)).T)
        coeffs = [None, -pca.components_[pc][0] / pca.components_[pc][1]]
    return coeffs

def get_thickness(img): return img.sum()

def get_aspect_ratio(img):
    rows, cols = np.where(img > 0.5)
    height = max(rows) - min(rows)
    width = max(cols) - min(cols)
    return width/height

def execute_objective(img, objective, algorithm, normalise=False):
    if objective == 'rotation':
        value = get_coeffs(img, algorithm)[1]
        if normalise is True:
            value = np.arctan(value)
    elif objective == 'thickness':
        value = get_thickness(img)
        if normalise is True:
            value /= np.prod(img.shape)
    elif objective == 'aspect_ratio':
        value = get_aspect_ratio(img)
        if normalise is True:
            value = np.arctan(value)
    else:
        raise ValueError
    return value

class MNISTPredDataset(MNIST):
    def __init__(self, digit, root, objective, image_grad_alg=None, train=True, download=False, binarise=False,
                 pixel_threshold=False):
        super().__init__(root, train, download)
        self.train_data, self.train_labels = filter_digit(digit, self.train_data, self.train_labels)
        self.train_data = self.train_data.float()
        self.train_data = functional.pad(self.train_data, (6, 6, 6, 6))/255
        if binarise is True:
            self.train_data = np.random.binomial(1, self.train_data.numpy())
        elif pixel_threshold is True:
            self.train_data = (self.train_data.numpy() > 0.5).astype(np.float)

        if binarise is True or pixel_threshold is True:
            self.objective_values = np.array([execute_objective(img, objective, image_grad_alg)
                                              for img in self.train_data])
            self.train_data = torch.Tensor(self.train_data)
        else:
            self.objective_values = np.array([execute_objective(img, objective, image_grad_alg)
                                              for img in self.train_data.numpy()])

    def __getitem__(self, index):
        img, target = self.train_data[index].reshape(1, 40, 40), self.train_labels[index]
        objective_value = self.objective_values[index]
        return img, target, objective_value

def train(data_path, digit, objective, image_grad_alg, num_epochs, batch_size, latent_size, dim_h, dim_intermediate,
          binarise, pixel_threshold, gpu, debug, save_path, load, vae_type):
    if save_path is not None and load is False:
        os.mkdir(save_path)
    train_gen = get_pred_generator(data_path, batch_size, binarise, digit, objective, image_grad_alg, pixel_threshold)
    if vae_type == 'conv':
        model = VAEPredictor(latent_size, dim_h, gpu)
    else:
        model = VAEPredictor2(latent_size, dim_h, gpu=gpu, dim_intermediate=dim_intermediate)
    if load is True:
        model.load_state_dict(torch.load(os.path.join(save_path, 'model')))
    if gpu:
        model = model.cuda()
    opt = optim.Adam(model.parameters())
    for epoch in range(1, num_epochs + 1):
        print('Epoch {}'.format(epoch))
        model.train()
        cum_loss = 0
        for train_data, _, objective_target in train_gen:
            if gpu:
                train_data = train_data.cuda()
                objective_target = objective_target.cuda()
            opt.zero_grad()
            dec_mean, dec_logvar, enc_mean, encoder_logvar, predicted_obj = model.forward(train_data)
            elbo = elbo_loss(enc_mean, encoder_logvar, train_data, dec_mean, dec_logvar)
            if objective == 'thickness': predicted_obj = torch.sigmoid(predicted_obj) * 255
            predictor_loss = (predicted_obj.flatten() - objective_target.float())**2
            loss = elbo + predictor_loss
            cum_loss += loss.sum().cpu().item()
            loss = loss.mean()
            if debug:
                print(loss, flush=True)
            loss.backward()
            opt.step()
        train_loss = cum_loss / len(train_gen.dataset)
        print('Training loss: {}'.format(train_loss), flush=True)

        if save_path is not None:
            torch.save(model.state_dict(), os.path.join(save_path, 'model'))


parser = ArgumentParser()
parser.add_argument('--data-path', type=str, default='aff_mnist')
parser.add_argument('--batch-size', type=int, default=32)
parser.add_argument('--num-epochs', type=int, default=1)
parser.add_argument('--objective', choices=['rotation', 'thickness', 'aspect_ratio'])
parser.add_argument('--image-grad-alg', choices=['least_squares', 'PCA'])
parser.add_argument('--gpu', action='store_true')
parser.add_argument('--debug', action='store_true')
parser.add_argument('--save-path', type=str, default=None)
parser.add_argument('--latent-size', type=int, default=20)
parser.add_argument('--dim-h', type=int, default=25)
parser.add_argument('--dim-intermediate', type=int, default=512)
parser.add_argument('--digit', type=int, default=None)
parser.add_argument('--binarise', action='store_true')
parser.add_argument('--load', action='store_true')
parser.add_argument('--vae-type', choices=['conv', 'conv2'])
parser.add_argument('--pixel-threshold', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    kwargs = vars(args)
    train(**kwargs)
