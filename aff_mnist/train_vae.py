from argparse import ArgumentParser
import os
import torch
import torch.optim as optim
from torchvision.datasets import MNIST
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor

from aff_mnist.train import get_data_generator
from aff_mnist.vae import VAEConv, VAEConv2, VAEFC, elbo_loss, get_iwae_lb

def train(data_path, digit, train_end, num_epochs, batch_size, latent_size, dim_h, gpu, vae_type, mode,
          binarise, debug, save_path, k, pixel_threshold):
    mnist = False
    if mnist is True:
        train_gen = DataLoader(MNIST('aff_mnist', download=True, transform=ToTensor()), batch_size)
        val_gen = DataLoader(MNIST('aff_mnist', download=True, train=False, transform=ToTensor()), batch_size)
    else:
        train_gen = get_data_generator(data_path, 1, train_end, batch_size, binarise, digit, pixel_threshold)
    if save_path is not None:
        os.mkdir(save_path)
    if vae_type == 'conv':
        model = VAEConv(latent_size, dim_h, gpu, mode=mode, k=k)
    elif vae_type == 'conv2':
        model = VAEConv2(latent_size, dim_h, gpu, mode=mode, k=k)
    elif vae_type == 'fc':
        model = VAEFC(latent_size, gpu, mode=mode)

    if gpu:
        model = model.cuda()
    opt = optim.Adam(model.parameters())
    for epoch in range(1, num_epochs + 1):
        print('Epoch {}'.format(epoch))
        model.train()
        cum_loss, correct = 0, 0
        for train_data, _ in train_gen:
            if gpu:
                train_data = train_data.cuda()
            opt.zero_grad()
            if model.mode == 'IWAE':
                dec_mean, dec_logvar, enc_mean, encoder_logvar, eps, z = model.forward(train_data)
                loss = -get_iwae_lb(train_data.squeeze(1).expand(model.k, train_data.shape[0], train_data.shape[2],
                                                      train_data.shape[3]), encoder_logvar, dec_mean, eps, z)
            else:
                dec_mean, dec_logvar, enc_mean, encoder_logvar = model.forward(train_data)
                loss = elbo_loss(enc_mean, encoder_logvar, train_data, dec_mean, dec_logvar)
            cum_loss += loss.sum().cpu().item()
            loss = loss.mean()
            if debug:
                print(loss, flush=True)
            loss.backward()
            opt.step()
        train_loss = cum_loss / len(train_gen.dataset)
        print('Training loss: {}'.format(train_loss), flush=True)

        if save_path is not None:
            torch.save(model.state_dict(), os.path.join(save_path, 'model'))

parser = ArgumentParser()
parser.add_argument('--data-path', type=str, default='training_and_validation_batches')
parser.add_argument('--train-end', type=int, default=33)
parser.add_argument('--batch-size', type=int, default=32)
parser.add_argument('--num-epochs', type=int, default=1)
parser.add_argument('--gpu', action='store_true')
parser.add_argument('--debug', action='store_true')
parser.add_argument('--save-path', type=str, default=None)
parser.add_argument('--latent-size', type=int, default=20)
parser.add_argument('--dim-h', type=int, default=25)
parser.add_argument('--vae-type', type=str, choices=['conv', 'conv2', 'fc'])
parser.add_argument('--digit', type=int, default=None)
parser.add_argument('--binarise', action='store_true')
parser.add_argument('--mode', choices=['VAE', 'IWAE'])
parser.add_argument('--k', type=int, default=None)
parser.add_argument('--pixel-threshold', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    kwargs = vars(args)
    train(**kwargs)
