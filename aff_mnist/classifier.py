from torch import nn
import torch.nn.functional as F

class Classifier(nn.Module):
    def __init__(self, out_dim=10):
        super().__init__()
        self.out_dim = out_dim
        self.conv1 = nn.Conv2d(1, 10, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(10, 20, 5)
        self.conv3 = nn.Conv2d(20, 30, 3)
        self.activation = F.relu
        self.fc1 = nn.Linear(120, 50)
        self.fc2 = nn.Linear(50, self.out_dim)
        if self.out_dim == 1:
            self.loss_fnc = nn.BCELoss(reduction='none')
        else:
            self.loss_fnc = nn.CrossEntropyLoss(reduction='none')

    def forward(self, x):
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = self.pool(self.conv3(x))
        x = x.view(x.shape[0], -1)
        x = self.activation(self.fc1(x))
        x = self.fc2(x)
        if self.out_dim == 1:
            x = F.sigmoid(x)
        return x

    def loss(self, predictions, labels):
        loss = self.loss_fnc(predictions, labels)
        return loss


class ConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3):
        super().__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size)
        self.bn = nn.BatchNorm2d(num_features=out_channels)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)

        return x
