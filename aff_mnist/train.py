import os, pickle
import numpy as np
import torch
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
import torch.nn.functional as F
from argparse import ArgumentParser

from aff_mnist.classifier import Classifier

def get_data_generator(data_path, start, end, batch_size, binarise=False, digit=None, pixel_threshold=False):
    if start == end: return None
    data, labels = [], []
    for i in range(start, end):
        with open(os.path.join(data_path, '{}.p'.format(i)), 'rb') as f:
            mat = pickle.load(f)
        file_data = mat[2].T/255
        file_labels = mat[5].reshape(-1, 1)
        if digit is not None:
            file_data, file_labels = filter_digit(digit, file_data, file_labels)
        if binarise is True:
            file_data = np.random.binomial(1, file_data)
        elif pixel_threshold is True:
            file_data = (file_data > 0.5).astype(np.float)
        data.append(file_data)
        labels.append(file_labels)
    data = np.concatenate(data)
    labels = np.vstack(labels)
    dataset = AffNISTDataset(data, labels)
    generator = DataLoader(dataset, batch_size)
    return generator

def filter_digit(digit, data, labels):
    indices = np.where(labels == digit)[0]
    data = data[indices]
    labels = labels[indices]
    return data, labels

def get_realvsgen_generators(num_train, digit, train_end, data_path, gen_data_path, batch_size):
    real_gen = get_data_generator(data_path, 1, train_end, batch_size=1, digit=digit)
    real_data, real_labels = real_gen.dataset.data.numpy(), real_gen.dataset.labels
    real_labels = np.ones_like(real_labels, dtype=np.float32)
    real_dataset = np.concatenate((real_data, real_labels), 1)
    gen_data = np.load(gen_data_path)
    gen_labels = np.zeros(gen_data.shape[0], dtype=np.float32).reshape(-1, 1)
    gen_dataset = np.concatenate((gen_data, gen_labels), 1)
    full_data_matrix = np.vstack((real_dataset, gen_dataset))
    for i in range(3): np.random.shuffle(full_data_matrix)

    if num_train >= full_data_matrix.shape[0] or num_train == -1:
        full_data_train = full_data_matrix[:, :-1]
        full_labels_train = full_data_matrix[:, -1]
        full_dataset_train = AffNISTDataset(full_data_train, full_labels_train)
        data_gen_train = DataLoader(full_dataset_train, batch_size)
        data_gen_val = None
    else:
        full_data_train = full_data_matrix[:num_train, :-1]
        full_labels_train = full_data_matrix[:num_train, -1]
        full_dataset_train = AffNISTDataset(full_data_train, full_labels_train)
        data_gen_train = DataLoader(full_dataset_train, batch_size)
        full_data_val = full_data_matrix[num_train:, :-1]
        full_labels_val = full_data_matrix[num_train:, -1]
        full_dataset_val = AffNISTDataset(full_data_val, full_labels_val)
        data_gen_val = DataLoader(full_dataset_val, batch_size)

    return data_gen_train, data_gen_val
    pass

class AffNISTDataset(Dataset):
    def __init__(self, data, labels):
        self.data = torch.from_numpy(data).float()
        self.labels = torch.from_numpy(labels)

    def __getitem__(self, index):
        data = self.data[index].reshape(1, 40, 40)
        label = self.labels[index]
        return data, label

    def __len__(self):
        return self.data.shape[0]

def train(data_path, gen_data_path, classifier_type, digit, num_train, train_end, val_end, num_epochs, batch_size, gpu,
          mnist, binarise, pixel_threshold, debug, save_path):
    if save_path is not None:
        os.mkdir(save_path)
    if classifier_type == 'digits':
        output_dim = 10
        if mnist is True:
            train_gen = DataLoader(MNIST('aff_mnist', download=True, transform=ToTensor()), batch_size)
            val_gen = DataLoader(MNIST('aff_mnist', download=True, train=False, transform=ToTensor()), batch_size)
        else:
            train_gen = get_data_generator(data_path, 1, train_end, batch_size, binarise,
                                           pixel_threshold=pixel_threshold)
            val_gen = get_data_generator(data_path, train_end, val_end, batch_size, binarise,
                                         pixel_threshold=pixel_threshold)
    else:
        output_dim = 1
        train_gen, val_gen = get_realvsgen_generators(num_train, digit, train_end, data_path, gen_data_path, batch_size)
    model = Classifier(output_dim)
    if gpu:
        model = model.cuda()
    opt = optim.Adam(model.parameters())
    best_val_loss = 9999
    for epoch in range(1, num_epochs+1):
        print('Epoch {}'.format(epoch))
        model.train()
        cum_loss, correct = 0, 0
        for train_data, train_labels in train_gen:
            if gpu:
                train_data = train_data.cuda()
                train_labels = train_labels.cuda()
            opt.zero_grad()
            predictions = model.forward(train_data)
            if classifier_type == 'digits':
                loss = model.loss(predictions, train_labels.view(-1))
                softmax_preds = F.softmax(predictions.detach(), 1)
                correct += (torch.max(softmax_preds, 1)[1] == train_labels.view(-1).long()).sum().cpu().item()
            else:
                loss = model.loss(predictions, train_labels)
                correct += ((predictions > 0.5) == train_labels.view(-1, 1).byte()).sum().cpu().item()
            cum_loss += loss.sum().cpu().item()
            loss = loss.mean()
            if debug:
                print(loss, flush=True)
            loss.backward()
            opt.step()
        train_loss = cum_loss/len(train_gen.dataset)
        train_acc = correct/len(train_gen.dataset)
        print('Training loss: {}'.format(train_loss), flush=True)
        print('Training accuracy: {}'.format(train_acc), flush=True)

        if val_gen is None:
            torch.save(model.state_dict(), os.path.join(save_path, 'model'))
            continue

        model.eval()
        with torch.no_grad():
            cum_loss, correct = 0, 0
            for val_data, val_labels in val_gen:
                if gpu:
                    val_data = val_data.cuda()
                    val_labels = val_labels.cuda()
                predictions = model.forward(val_data)
                if classifier_type == 'digits':
                    loss = model.loss(predictions, val_labels.view(-1))
                    softmax_preds = F.softmax(predictions.detach(), 1)
                    correct += (torch.max(softmax_preds, 1)[1] == val_labels.view(-1)).sum().cpu().item()
                else:
                    loss = model.loss(predictions, val_labels)
                    correct += ((predictions > 0.5) == val_labels.view(-1, 1).byte()).sum().cpu().item()
                cum_loss += loss.sum().cpu().item()
        val_loss = cum_loss/len(val_gen.dataset)
        val_acc = correct/len(val_gen.dataset)
        print('Validation loss: {}'.format(val_loss), flush=True)
        print('Validation accuracy: {}'.format(val_acc), flush=True)
        if save_path is not None and val_loss < best_val_loss:
            torch.save(model.state_dict(), os.path.join(save_path, 'best_model'))
            best_val_loss = val_loss
            print('Current best epoch: {}'.format(epoch), flush=True)
        

parser = ArgumentParser()
parser.add_argument('--data-path', type=str, default='training_and_validation_batches')
parser.add_argument('--gen-data-path', type=str, default='generated_padded_mnist.npy')
parser.add_argument('--train-end', type=int, default=27)
parser.add_argument('--val-end', type=int, default=33)
parser.add_argument('--batch-size', type=int, default=32)
parser.add_argument('--num-epochs', type=int, default=1)
parser.add_argument('--gpu', action='store_true')
parser.add_argument('--debug', action='store_true')
parser.add_argument('--mnist', action='store_true')
parser.add_argument('--save-path', type=str, default=None)
parser.add_argument('--binarise', action='store_true')
parser.add_argument('--classifier-type', choices=['digits', 'realvsgen'])
parser.add_argument('--digit', type=int, default=None)
parser.add_argument('--num-train', type=int, default=9800)
parser.add_argument('--pixel-threshold', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    kwargs = vars(args)
    train(**kwargs)
