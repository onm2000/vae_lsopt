import torch
import torch.utils.data
from torch import nn, optim
from torch.autograd import Variable
from torch.nn import functional as F
import numpy as np
from aff_mnist.classifier import ConvBlock


# from torchvision.utils import save_image

class VAEBase(nn.Module):
    def __init__(self, mode='VAE', k=None):
        super().__init__()
        self.mode = mode
        self.k = k

    def sample(self):
        z = Variable(torch.randn(self.latent), requires_grad=True)
        if self.gpu is True:
            z = z.cuda()
        return z

    def repar(self, mu, logvar):
        eps = self.sample()
        samp = F.mul((0.5 * logvar).exp(), eps)
        samp = samp + mu
        if self.mode == 'IWAE':
            return eps, samp
        else:
            return samp

    def forward(self, x):

        # forward pass (take your image, get its params, reparamaterize the N(0,1) with them, decode and output)
        mu, logvar = self.encode(x)
        if self.mode == 'IWAE':
            mu = mu.expand(self.k, mu.shape[0], mu.shape[1])
            logvar = logvar.expand(self.k, logvar.shape[0], logvar.shape[1])
            eps, z = self.repar(mu, logvar)
            eps = eps.expand(self.k, z.shape[1], eps.shape[0])
            om, ov = self.decode(z)
            return om, ov, mu, logvar, eps, z
        else:
            decode_info = self.repar(mu, logvar)
            om, ov = self.decode(decode_info)
        return om, ov, mu, logvar

class VAEConv(VAEBase):

    # MAIN VAE Class

    def __init__(self, latent_size=20, dim_h=25, gpu=False, mode='VAE', k=None):
        super().__init__(mode=mode, k=k)
        self.gpu = gpu
        self.act_f = F.relu
        self.h = 40
        self.w = 40
        self.u = 500

        self.latent = latent_size
        self.dim_h = dim_h
         
        # encoder
        self.conv1 = nn.Conv2d(1, 10, 5)
        self.conv2 = nn.Conv2d(10, 20, 5)
        self.conv3 = nn.Conv2d(20, 30, 3)
        self.activation = F.relu
        self.fc1 = nn.Linear(120, 80)
        self.fc2 = nn.Linear(80, 2*latent_size)
        self.pool = nn.MaxPool2d(2, 2)


        # decoder
        self.fc3 = nn.Linear(latent_size, self.dim_h)
        self.convt1 = nn.ConvTranspose2d(self.dim_h, 20, 6, stride=2)
        self.convt2 = nn.ConvTranspose2d(20, 10, 7, stride=2)
        self.convt3 = nn.ConvTranspose2d(10, 1, 8, stride=2)
        """
        self.conv1 = nn.Conv2d(1, 32, 3, stride=2, padding=1)
        self.conv2 = nn.Conv2d(32, 64, 3, stride=2, padding=1)
        self.fc1 = nn.Linear(3136, 16)
        self.fc2 = nn.Linear(16, 4)#2*latent_size)

        self.fc3 = nn.Linear(latent_size, 3136)
        self.convt1 = nn.ConvTranspose2d(64, 64, 3, stride=2, padding=1, output_padding=1)
        self.convt2 = nn.ConvTranspose2d(64, 32, 3, stride=2, padding=1, output_padding=1)
        self.convt3 = nn.ConvTranspose2d(32, 1, 3, padding=1)
        self.activation = F.relu
        """

    def encode(self, x):
        
        x = self.pool(self.activation(self.conv1(x)))
        x = self.pool(self.activation(self.conv2(x)))
        x = self.pool(self.activation(self.conv3(x)))
        x = x.view(x.shape[0], -1)
        x = self.activation(self.fc1(x))
        x = self.fc2(x)
        """
        x = self.activation(self.conv1(x))
        x = self.activation(self.conv2(x))
        x = x.view(x.shape[0], -1)
        x = self.activation(self.fc1(x))
        x = self.fc2(x)
        """
        mu = x[:, :self.latent]
        logvar = x[:, self.latent:]
        return mu, logvar

    def decode(self, x):
        x = self.fc3(x)
        if self.mode =='IWAE':
            x = x.view((x.shape[0] * x.shape[1], self.dim_h))
        x = x.view(x.shape[0], self.dim_h, 1, 1)
        #x = x.view(-1, 64, 7, 7)
        x = self.activation(self.convt1(x))
        x = self.activation(self.convt2(x))
        im = F.sigmoid(self.convt3(x))
        if self.mode == 'IWAE':
            im = im.view(self.k, -1, im.shape[-2], im.shape[-1])
        return im, []


class VAEConv2(VAEBase):

    # MAIN VAE Class

    def __init__(self, latent_size=20, dim_h=25, dim_intermediate=800, gpu=False, mode='VAE', k=None):
        super().__init__(mode=mode, k=k)
        self.gpu = gpu
        self.latent = latent_size
        self.dim_h = dim_h
        self.dim_intermediate = dim_intermediate # 512 for original mnist, 800 for affnist

        # encoder
        #self.conv0 = nn.Conv2d(1, 16, 13)
        in_height, in_width, stride, filter_size = 40, 40, 2, 5
        pad_top, pad_bottom, pad_left, pad_right = get_conv_padding(in_height, in_width, stride, filter_size)
        self.pade1 = nn.ZeroPad2d((pad_top, pad_bottom, pad_left, pad_right))
        self.conv1 = nn.Conv2d(1, 16, 5, stride=2)

        in_height, in_width, stride, filter_size = 20, 20, 2, 5
        pad_top, pad_bottom, pad_left, pad_right = get_conv_padding(in_height, in_width, stride, filter_size)
        self.pade2 = nn.ZeroPad2d((pad_top, pad_bottom, pad_left, pad_right))
        self.conv2 = nn.Conv2d(16, 32, 5, stride=2)

        in_height, in_width, stride, filter_size = 10, 10, 2, 5
        pad_top, pad_bottom, pad_left, pad_right = get_conv_padding(in_height, in_width, stride, filter_size)
        self.pade3 = nn.ZeroPad2d((pad_top, pad_bottom, pad_left, pad_right))
        self.conv3 = nn.Conv2d(32, 32, 5, stride=2)

        self.activation = F.relu
        self.fce1 = nn.Linear(self.dim_intermediate, dim_h)
        self.fce2 = nn.Linear(dim_h, 2 * latent_size)

        # decoder
        self.fcd1 = nn.Linear(self.latent, self.dim_h)
        self.fcd2 = nn.Linear(self.dim_h, self.dim_intermediate)
        def up_dim(output_size, input_size): return int(np.ceil(output_size / float(input_size)))

        self.convt1 = nn.ConvTranspose2d(32, 32, 5, stride=(up_dim(10, 5), up_dim(10, 5)), padding=2, output_padding=1)
        self.convt2 = nn.ConvTranspose2d(32, 16, 5, stride=(up_dim(20, 10), up_dim(20, 10)), padding=2, output_padding=1)
        self.convt3 = nn.ConvTranspose2d(16, 1, 5, stride=(up_dim(40, 20), up_dim(40, 20)), padding=2, output_padding=1)


    def encode(self, x):
        #x = x[:, :, :28, :28]
        #x = self.activation(self.conv0(x))
        x = self.pade1(x)
        x = self.activation(self.conv1(x))
        x = self.pade2(x)
        x = self.activation(self.conv2(x))
        x = self.pade3(x)
        x = self.activation(self.conv3(x))
        x = x.view(x.shape[0], -1)
        x = self.activation(self.fce1(x))
        x = self.fce2(x)
        mu = x[:, :self.latent]
        logvar = x[:, self.latent:]
        return mu, logvar

    def decode(self, x):
        x = self.activation(self.fcd1(x))
        x = self.activation(self.fcd2(x))
        if self.mode == 'IWAE':
            x = x.view((x.shape[0] * x.shape[1], x.shape[2]))
        num_filters = 32
        x = x.view(x.shape[0], num_filters, int((x.shape[1] / num_filters) ** 0.5), int((x.shape[1] / num_filters) ** 0.5))
        x = self.activation(self.convt1(x))
        x = self.activation(self.convt2(x))
        x = F.sigmoid(self.convt3(x))
        if self.mode == 'IWAE':
            x = x.view(self.k, -1, x.shape[-2], x.shape[-1])
        return x, []

class VAEFC(VAEBase):
    def __init__(self, latent_size=20, gpu=False, mode='VAE', k=None):
        super().__init__(mode=mode, k=k)
        self.gpu = gpu
        self.h = 40
        self.w = 40
        self.u = 500

        self.latent = latent_size
        self.fc1 = nn.Linear(self.h*self.w, 800)
        self.fc2 = nn.Linear(800, 200)
        self.fc3 = nn.Linear(200, 2*self.latent)
        self.encoder = nn.Sequential(self.fc1, nn.ReLU(), self.fc2, nn.ReLU(), self.fc3)

        self.fco1 = nn.Linear(self.latent, 200)
        self.fco2 = nn.Linear(200, 800)
        self.fco3 = nn.Linear(800, self.h*self.w)
        self.decoder = nn.Sequential(self.fco1, nn.ReLU(), self.fco2, nn.ReLU(), self.fco3, nn.Sigmoid())

    def encode(self, x):
        x = x.reshape(x.shape[0], -1)
        x = self.encoder(x)
        mu = x[:, :self.latent]
        logvar = x[:, self.latent:]
        return mu, logvar

    def decode(self, x):
        x = self.decoder(x)
        x = x.reshape(-1, self.h*self.w)
        return x, []

class VAEPredictor(VAEConv):
    def __init__(self, latent_size=20, dim_h=25, gpu=False, mode='VAE', k=None):
        super().__init__(latent_size, dim_h, gpu, mode=mode, k=k)
        self.predictor = nn.Sequential(nn.Linear(latent_size, int(latent_size/2)), nn.ReLU(),
                                       nn.Linear(int(latent_size/2), 1))



    def forward(self, x):
        # forward pass (take your image, get its params, reparamaterize the N(0,1) with them, decode and output)
        mu, logvar = self.encode(x)
        decode_info = self.repar(mu, logvar)
        predicted_objective = self.predictor(decode_info)
        om, ov = self.decode(decode_info)
        return om, ov, mu, logvar, predicted_objective

class VAEPredictor2(VAEConv2):
    def __init__(self, latent_size=20, dim_h=25, dim_intermediate=512, gpu=False, mode='VAE', k=None, normalise=False,
                 objective=None):
        super().__init__(latent_size, dim_h, gpu=gpu, dim_intermediate=dim_intermediate, mode=mode, k=k)
        self.predictor = nn.Sequential(nn.Linear(latent_size, int(latent_size/2)), nn.ReLU(),
                                       nn.Linear(int(latent_size/2), 1))
        self.normalise = normalise
        self.objective = objective


    def forward(self, x):
        # forward pass (take your image, get its params, reparamaterize the N(0,1) with them, decode and output)
        mu, logvar = self.encode(x)
        decode_info = self.repar(mu, logvar)
        predicted_objective = self.predictor(decode_info)
        if self.normalise is True:
            if self.objective == 'rotation':
                predicted_objective = torch.arctan(predicted_objective)
            elif self.objective == 'thickness':
                predicted_objective = torch.sigmoid(predicted_objective)
            elif self.objective == 'aspect_ratio':
                predicted_objective = torch.arctan(predicted_objective)
        om, ov = self.decode(decode_info)
        return om, ov, mu, logvar, predicted_objective

def elbo_loss(enc_m, enc_v, x, dec_m, dec_v, beta=1):
    KL_part = 0.5 * (torch.sum(enc_v.exp() + enc_m * enc_m - 1 - enc_v, 1))

    # assume MNIST, therefore 'pseudo-binary' and use Bernoulli
    Recon_total = F.binary_cross_entropy(dec_m.view(dec_m.shape[0], -1), x.view(x.shape[0], -1), reduction='none').sum(dim=1)
    output = (Recon_total + beta * KL_part)  # ave loss per datapoint

    return output

def get_iwae_lb(x, enc_logv, dec_m, eps, z):
    log_recon = torch.sum( torch.sum(x * torch.log(dec_m + 0.0001) + (1 - x) * torch.log(1 - dec_m + 0.0001), -1), -1)
    log_prior = torch.sum(-0.5 * z**2, -1)
    log_posterior = torch.sum(-0.5 * (eps)**2 - enc_logv, -1)

    log_weight = log_recon+log_prior-log_posterior
    log_weight = log_weight - torch.max(log_weight, 0)[0]
    weight = torch.exp(log_weight)
    weight = weight / torch.sum(weight, 0)
    weight = weight.detach()
    lb = torch.sum(weight * (log_recon+log_prior-log_posterior), 0)
    return lb

def get_conv_padding(in_height, in_width, stride, filter_size):
    out_height = np.ceil(float(in_height) / float(stride))
    out_width = np.ceil(float(in_width) / float(stride))
    pad_along_height = max((out_height - 1) * stride + filter_size - in_height, 0)
    pad_along_width = max((out_width - 1) * stride + filter_size - in_width, 0)
    pad_top = int(pad_along_height // 2)
    pad_bottom = int(pad_along_height - pad_top)
    pad_left = int(pad_along_width // 2)
    pad_right = int(pad_along_width - pad_left)
    return pad_top, pad_bottom, pad_left, pad_right

def get_convt_padding(in_height, in_width, out_height, out_width, stride, filter_size):
    padding_height = (stride * (in_height - 1) + filter_size - out_height) / 2
    padding_width = (stride * (in_width - 1) + filter_size - out_width) / 2
    pad_top = int(np.floor(padding_height))
    pad_bottom = int(np.ceil(padding_height))
    pad_left = int(np.floor(padding_width))
    pad_right = int(np.ceil(padding_width))
    return pad_top, pad_bottom, pad_left, pad_right
