import argparse
import torch
import numpy as np
import nmslib
import os

from gmm_parameters import load_parameters

def get_random_grid(endpoints, num_points):
    """
    sample points uniformly at random from a multidimensional space
    :param endpoints: list of tuples, each tuple containing the boundaries of the space for a dimension
    :param num_points: number of points to sample
    :return: a 2D array, each row of which is a sampled point in the multidimensional space
    """
    num_dims = len(endpoints)
    low, high = list(zip(*endpoints))
    low = np.array(low)
    high = np.array(high)
    grid_points = np.random.uniform(low, high, (num_points, num_dims))

    return grid_points

def edge_grid(endpoints, num_points):
    num_dims = len(endpoints)
    low, high = list(zip(*endpoints))
    grid_points = np.zeros(shape=(1, num_dims))
    for n in range(num_points):
        selector = np.random.choice(np.array([-1, 1]), size=num_dims)
        point_low, point_high = [], []
        for d in range(num_dims):
            if selector[d] == 1:
                point_low.append(low[d])
                point_high.append(high[d])
            else:
                point_low.append(-high[d])
                point_high.append(-low[d])
        point = np.random.uniform(point_low, point_high, (1, num_dims))
        grid_points = np.vstack((grid_points, point))
    return grid_points[1:]

def create_knn_index(means, index_params={}, load_index=False, index_loc=None):
    index = nmslib.init(space='l2', data_type=nmslib.DataType.DENSE_VECTOR)
    index.addDataPointBatch(means)
    if load_index is True:
        index.loadIndex(index_loc)
    else:
        index.createIndex(index_params, print_progress=True)
        if index_loc:
            index.saveIndex(index_loc)
    return index

def prepare_parameters(all_grid_points, means, variances, batch_size, log_nc=False, asnumpy=False, gpu=False):
    num_datapoints, dimensionality = all_grid_points.shape
    num_means = means.shape[0]
    num_batches = num_datapoints / batch_size
    if num_batches.is_integer():
        num_batches = int(num_batches)
    else:
        num_batches = int(num_batches) + 1
    try:
        means = means.expand_dims(0)
        variances = variances.expand_dims(0)
    except:
        means = means.unsqueeze(0)
        variances = variances.unsqueeze(0)
    all_grid_points = all_grid_points.reshape((num_datapoints, 1, dimensionality))

    densities = torch.zeros(1)
    if log_nc is True:
        normalising_constant = np.expand_dims(-0.5 * dimensionality * np.log(2*np.pi) - 0.5 * (np.sum(np.log(variances.numpy()), axis=2)), -1)
    else:
        normalising_constant = np.expand_dims(np.sqrt((2 * np.pi) ** dimensionality) * np.sqrt(
                                            np.prod(variances.numpy(), axis=2)), -1)

    if asnumpy is True:
        means = means.numpy()
        variances = variances.numpy()
        all_grid_points = all_grid_points.numpy()
        densities = densities.numpy()
    else:
        normalising_constant = torch.from_numpy(normalising_constant)
        if gpu:
            normalising_constant = normalising_constant.cuda()
            means = means.cuda()
            variances = variances.cuda()
            all_grid_points = all_grid_points.cuda()

    return means, variances, all_grid_points, densities, normalising_constant, num_batches, num_datapoints,\
           dimensionality, num_means

def batch_boundaries(batch_num, num_batches, batch_size, num_datapoints):
    start = batch_num * batch_size
    if batch_num < num_batches - 1:
        end = (batch_num + 1) * batch_size
    else:
        end = num_datapoints
        batch_size = num_datapoints - start
    return start, end, batch_size

def log_gmm_density_np(all_grid_points, means, variances, batch_size=1):
    means, variances, all_grid_points, densities, log_normalising_constant, num_batches, num_datapoints, \
    dimensionality, num_means = prepare_parameters(all_grid_points, means, variances, batch_size, log_nc=True, asnumpy=True)

    for batch_num in range(num_batches):
        start, end, batch_size = batch_boundaries(batch_num, num_batches, batch_size, num_datapoints)
        grid_points = all_grid_points[start:end, :, :]
        exponents = ((means - grid_points) ** 2 / (-2 * variances)).sum(axis=2).reshape(batch_size, num_means, 1)
        log_gaussian_densities = exponents - log_normalising_constant
        overflow_const = np.max(log_gaussian_densities, axis=1)
        log_gmm_densities = np.log((np.exp(log_gaussian_densities - overflow_const)).sum(axis=1)) + overflow_const - \
            np.log(num_means)

        densities = np.concatenate([densities, log_gmm_densities.reshape((-1,))])
    densities = densities[1:]
    return densities

def log_gmm_density_torch(all_grid_points, means, variances, batch_size=128, gpu=True):
    means, variances, all_grid_points, densities, log_normalising_constant, num_batches, num_datapoints, \
    dimensionality, num_means = prepare_parameters(all_grid_points, means, variances, batch_size, log_nc=True, gpu=gpu)
    for batch_num in range(num_batches):
        start, end, batch_size = batch_boundaries(batch_num, num_batches, batch_size, num_datapoints)
        grid_points = all_grid_points[start:end, :, :]
        exponents = ((means - grid_points.float()) ** 2 / (-2 * variances)).sum(dim=2).reshape((batch_size, num_means, 1))
        log_gaussian_densities = exponents - log_normalising_constant
        overflow_const = torch.max(log_gaussian_densities, dim=1)[0].float().reshape((batch_size, 1, 1))
        log_num_means = torch.log(torch.tensor([num_means]).float())
        if gpu:
            log_num_means = log_num_means.cuda()
        log_gmm_densities = torch.log((torch.exp(log_gaussian_densities - overflow_const)).sum(dim=1)).reshape((-1,)) + \
                             overflow_const.reshape((-1,)) - log_num_means
        log_gmm_densities = log_gmm_densities.cpu()
        densities = torch.cat([densities, log_gmm_densities.reshape((-1,))])
        print(end, flush=True)

    densities = densities[1:]
    return densities

def get_most_valid_points(means, variances, grid_points, index, num_neighbours=50, threshold=0.3):
    log_densities = approximate_densities(index, means, variances, num_neighbours, grid_points)
    valid_points_indices = np.where(log_densities > threshold)
    if valid_points_indices:
        valid_points = grid_points[valid_points_indices]
        valid_point_densities = log_densities[valid_points_indices]
    else:
        print('No valid points found')
        valid_points, valid_point_densities = None, None
    return valid_points, valid_point_densities, log_densities

def clean_parameters(means, variances):
    """
    remove rows with zero variances from means and variances
    """
    rows_to_keep = np.all(variances > 1e-6, axis=1)
    variances = variances[rows_to_keep, :].reshape((-1, variances.shape[1]))
    means = means[rows_to_keep, :].reshape((-1, variances.shape[1]))
    return means, variances

def main(num_gridpoints, grid_endpoints, num_neighbours, batch_size, save_dname, prior_variance=1, latent_dim=None,
         gridpoints_type='', densities_type='', gridpoints_path=None, densities_path=None, load_index=False, gpu=False):
    print('Loading parameters', flush=True)
    if densities_type == 'prior':
        means = np.zeros((1, latent_dim))
        variances = np.ones((1, latent_dim)) * prior_variance
    else:
        means, variances = load_parameters(save_dname)
        means, variances = clean_parameters(means, variances)

    if not os.path.exists(save_dname):
        os.mkdir(save_dname)

    if densities_type != 'true':
        index_params = {'efConstruction': 2000, 'M':100}
        print('Creating index', flush=True)
        index = create_knn_index(means, index_params, load_index, os.path.join(save_dname, 'index'))

    if gridpoints_type == 'load':
        grid_points = np.loadtxt(gridpoints_path)
    else:
        if gridpoints_type == 'progressive':
            grid_points = []
            for i in range(20):
                grid_endpoints = [(i/2, (i + 1)/2)] * variances.shape[1]
                points = edge_grid(grid_endpoints, int(num_gridpoints / 20))
                grid_points.append(points)
            grid_points = np.vstack(grid_points)
        elif gridpoints_type == 'edge':
            grid_points = edge_grid(grid_endpoints, num_gridpoints)
        elif gridpoints_type == 'prior':
            grid_points = np.random.normal(scale=np.sqrt(prior_variance), size=(num_gridpoints, latent_dim))
        elif gridpoints_type == 'means':
            grid_points = means
        else:
            if grid_endpoints is None:
                grid_endpoints = [(-1, 1)] * variances.shape[1]
            grid_points = get_random_grid(grid_endpoints, num_gridpoints)
        np.savetxt(gridpoints_path, grid_points)

    print('Grid points created', flush=True)
    if densities_type == 'k':
        k_vals, all_k_densities = different_k_densities(means, variances, grid_points, index)
        np.savetxt(os.path.join(save_dname, 'k_vals.txt'), k_vals)
        np.savetxt(densities_path, np.array(all_k_densities))
    elif densities_type == 'approx':
        densities = approximate_densities(index, means, variances, num_neighbours, grid_points)
        np.savetxt(densities_path, densities)
    elif densities_type in ('true', 'prior'):
        log_densities = log_gmm_density_torch(torch.from_numpy(grid_points), torch.from_numpy(means),
                                           torch.from_numpy(variances), batch_size=batch_size, gpu=gpu)
        try:
            np.savetxt(densities_path, log_densities)
        except:
            np.savetxt(densities_path, log_densities.numpy())
            print('numpy')

def different_k_densities(means, variances, grid_points, index):
    all_k_densities = []
    k_vals = list(range(1, 10)) + list(range(10, 100, 10)) + list(range(100, 500, 50)) + list(range(500, 1001, 100))
    for k in k_vals:
        print(k, flush=True)
        k_densities = approximate_densities(index, means, variances, k, grid_points)
        all_k_densities.append(k_densities)
    return k_vals, all_k_densities

def approximate_densities(index, means, variances, num_neighbours, grid_points):
    batch_neighbours = index.knnQueryBatch(grid_points, k=num_neighbours)
    print('Nearest neighbours found', flush=True)
    densities = []
    for i, point_neighbours in enumerate(batch_neighbours):
        indices = np.array(point_neighbours[0])
        density = log_gmm_density_np(torch.from_numpy(grid_points[i, :]).reshape((1, -1)), torch.from_numpy(means[indices]),
                                     torch.from_numpy(variances[indices])) + np.log(len(indices)) - np.log(means.shape[0])
        densities.append(float(density))
        if i % 10000 == 0:
            print(i+10000, flush=True)
    return np.array(densities)

parser = argparse.ArgumentParser()
parser.add_argument('--save-dname')
parser.add_argument('--gridpoints-path')
parser.add_argument('--densities-path')
parser.add_argument('--num-gridpoints', type=int, default=100000)
parser.add_argument('--grid-endpoints', type=int, nargs='+', default=None)
parser.add_argument('--num-neighbours', type=int, default=50)
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--load-index', action='store_true')
parser.add_argument('--densities-type', default='')
parser.add_argument('--gridpoints-type', default='')
parser.add_argument('--prior-variance', type=float, default=1.0)
parser.add_argument('--latent-dim', type=int, default=None)
parser.add_argument('--gpu', action='store_true')

if __name__ == '__main__':
    kwargs = vars(parser.parse_args())
    endpoints = kwargs['grid_endpoints']
    if endpoints is not None:
        grid_endpoints = []
        for i in range(0, len(endpoints), 2):
            grid_endpoints.append(tuple(endpoints[i:i+2]))
        kwargs['grid_endpoints'] = grid_endpoints
    main(**kwargs)
