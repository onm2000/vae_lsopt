import scipy.misc as smp
import os
import torch
import numpy as np
from argparse import ArgumentParser
from knn_grid_evaluation import batch_boundaries
from aff_mnist.vae import VAEPredictor

parser = ArgumentParser()
parser.add_argument('--batch-size', type=int, default=512)
parser.add_argument('--savedir', default='experiments/vae_pca_binarise/post_train/')
parser.add_argument('--gpu', action='store_true')
parser.add_argument('--decode', action='store_true')
parser.add_argument('--model-path')

args = parser.parse_args()

batch_size = args.batch_size
if args.decode:
    model = VAEPredictor(16, 32)
    model.load_state_dict(torch.load(args.model_path, map_location='cpu'))
    if args.gpu:
        model = model.cuda()
train_data = torch.Tensor(np.load('aff_mnist/training_and_validation_batches/all.npy'))/255
num_batches = train_data.shape[0] / batch_size
if num_batches.is_integer():
    num_batches = int(num_batches)
else:
    num_batches = int(num_batches) + 1
for threshold in range(-100, -10, 10):
    print(threshold)
    if args.decode:
        optima = torch.Tensor(np.loadtxt(os.path.join(args.savedir, 'threshold_{}.0/local_optima.txt'.format(threshold))))
        if args.gpu:
            optima = optima.cuda()
        images = model.decode(optima)[0].reshape(optima.shape[0], -1)
        del optima
    minima = []
    for i in range(1, 101):
        if args.decode:
            image = images[i-1, :]
        else:
            image = torch.Tensor(smp.imread(
                os.path.join(args.savedir, 'threshold_{}.0'.format(threshold), 'image_{}.jpg'.format(i)))
                ).view(-1)/255
            if args.gpu:
                image = image.cuda()
        image_errors = []
        for batch_num in range(num_batches):
            start, end, batch_size = batch_boundaries(batch_num, num_batches, batch_size, train_data.shape[0])
            data = train_data[start:end, :]
            if args.gpu:
                data = data.cuda()
            with torch.no_grad():
                batch_errors = (data - image).pow(2).sum(1).cpu()
            image_errors.append(batch_errors)
        image_errors = torch.cat(image_errors, 0)
        minimum = torch.min(image_errors).item()
        minima.append(minimum)
    if args.decode:
        np.savetxt(os.path.join(args.savedir, 'threshold_{}.0'.format(threshold), 'minima_decode.txt'), np.array(minima))
    else:
        np.savetxt(os.path.join(args.savedir, 'threshold_{}.0'.format(threshold), 'minima.txt'), np.array(minima))
