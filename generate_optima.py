import argparse
import numpy as np
import nlopt
import torch
import os
import scipy.misc as smp
from scipy.stats import multivariate_normal

from aff_mnist.vae import VAEPredictor, VAEPredictor2, VAEConv, VAEConv2, elbo_loss, get_iwae_lb
from aff_mnist.train_predictor_vae import execute_objective
from knn_grid_evaluation import approximate_densities, batch_boundaries, clean_parameters, \
    create_knn_index, log_gmm_density_torch
from gmm_parameters import load_parameters


def predict_score(c, mdl):
    c = torch.tensor(c)
    c = torch.cat(list(c)).reshape((1, -1))
    return float(mdl.predict_score(c).asnumpy())


def negative_score(c, mdl):
    return -predict_score(c, mdl)

def load(stem):
    try:
        return np.load('{}.npy'.format(stem))
    except:
        return np.loadtxt('{}.txt'.format(stem))

def predict_scores_by_batches(grid_points, batch_size, model):
    with torch.no_grad():
        grid_points = torch.tensor(grid_points).float()
        scores = np.array([0])
        num_datapoints = grid_points.shape[0]
        num_batches = num_datapoints / batch_size
        if num_batches.is_integer():
            num_batches = int(num_batches)
        else:
            num_batches = int(num_batches) + 1
        for batch_num in range(num_batches):
            start, end, batch_size = batch_boundaries(batch_num, num_batches, batch_size, num_datapoints)
            batch_scores = model.predictor(grid_points[start:end, :]).cpu().view(-1).numpy()
            scores = np.concatenate((scores, batch_scores))
            if batch_num % 10 == 0:
                print(end, flush=True)
    return scores[1:]

def get_loss(model, image):
    with torch.no_grad():
        if hasattr(model, 'mode') and model.mode == 'IWAE':
            dec_mean, dec_logvar, enc_mean, encoder_logvar, eps, z = model.forward(image)
            loss = -get_iwae_lb(image.squeeze(1).expand(model.k, image.shape[0], image.shape[2], image.shape[3]),
                                encoder_logvar, dec_mean, eps, z)
        else:
            dec_mean, dec_logvar, enc_mean, encoder_logvar = model.forward(image)
            loss = elbo_loss(enc_mean, encoder_logvar, image, dec_mean, dec_logvar)
    return loss

def main(latent_size, dim_h, load_dname, sd_path, affnist_sd_path, objective, vae_type, vae_mode='VAE', k=None, image_grad_alg=None,
    dim_intermediate=None, load_scores=False, threshold=None, maxeval=None, ftol_abs=None, stopval=None, t=1, batch_size=None,
    constraint_type='latent_dist', knn=False, num_neighbours=1000, gpu=False, load_index=False, DEBUG=False,
         gridpoints_path=None, log_densities_path=None):
    if threshold:
        save_dname = os.path.join(load_dname, constraint_type, 'threshold_{}'.format(threshold))
    else:
        save_dname = os.path.join(load_dname, constraint_type, 'no_threshold')
    if not os.path.isdir(os.path.join(load_dname, constraint_type)):
        os.mkdir(os.path.join(load_dname, constraint_type))
    if os.path.isdir(save_dname):
        pass#raise Exception
    else:
        os.mkdir(save_dname)
    if vae_type == 'conv':
        model = VAEPredictor(latent_size, dim_h, gpu)
    elif vae_type == 'conv2':
        model = VAEPredictor2(latent_size, dim_h, dim_intermediate, gpu)
    model.load_state_dict(torch.load(sd_path, map_location='cpu'))
    model.eval()
    if vae_type == 'conv':
        affnist_model = VAEConv(latent_size, dim_h, gpu, mode=vae_mode, k=k)
    elif vae_type == 'conv2':
        affnist_model = VAEConv2(latent_size, dim_h, dim_intermediate, gpu, mode=vae_mode, k=k)
    affnist_model.load_state_dict(torch.load(affnist_sd_path, map_location='cpu'))
    affnist_model.eval()
    print('Loading grid points', flush=True)
    valid_points = torch.from_numpy(np.loadtxt(gridpoints_path))
    log_densities = torch.from_numpy(np.loadtxt(log_densities_path))
    print('Getting scores', flush=True)
    if load_scores is True:
        scores = np.loadtxt(os.path.join(load_dname, 'grid_scores.txt'))
    else:
        if batch_size is None:
            scores = model.predictor(valid_points).cpu().numpy()
        else:
            scores = predict_scores_by_batches(valid_points, batch_size, model)
        np.savetxt(os.path.join(load_dname, 'grid_scores.txt'), scores)

    if threshold:
        scores = scores[np.where(log_densities > threshold)]
        valid_points = valid_points[np.where(log_densities > threshold)]

    print('Loading GMM parameters', flush=True)
    means, variances = load_parameters(load_dname)
    means, variances = clean_parameters(means, variances)

    if knn is True:
        index_params = {'efConstruction': 2000, 'M': 100}
        print('Creating index', flush=True)
        index = create_knn_index(means, index_params, load_index, os.path.join(load_dname, 'index'))

    def predict_score_nlopt(c, grad):
        c = torch.tensor(c).float()
        #c = torch.cat(list(c)).reshape((1, -1))
        c = c.view(1, -1)
        with torch.no_grad():
            score = model.predictor(c).cpu().item()
        if DEBUG is True:
            print(score, flush=True)
        return score

    def approximate_densities_nlopt(grid_point, grad, index, means, variances, num_neighbours, threshold):
        grid_point = grid_point.reshape(1, -1)
        densities = approximate_densities(index, means, variances, num_neighbours, grid_point)
        ret = float(-densities + threshold)
        return ret

    def exact_density_nlopt(grid_point, grad, means, variances, threshold):
        grid_point = grid_point.reshape(1, -1)
        density = log_gmm_density_torch(torch.from_numpy(grid_point), torch.from_numpy(means),
                              torch.from_numpy(variances), batch_size=1, gpu=gpu)
        ret = float(-density + threshold)
        return ret

    print('Optimising', flush=True)
    opt = nlopt.opt(nlopt.LN_COBYLA, valid_points.shape[1])
    opt.set_max_objective(predict_score_nlopt)
    if threshold:
        if constraint_type == 'latent_dist':
            if knn is True:
                opt.add_inequality_constraint(
                    lambda x, grad: approximate_densities_nlopt(x, grad, index, means, variances, num_neighbours,
                                                                threshold), 0)
            else:
                opt.add_inequality_constraint(
                lambda x, grad: exact_density_nlopt(x, grad, means, variances, threshold), 0)
        elif constraint_type == 'prior':
            opt.add_inequality_constraint(lambda x, grad: float(-multivariate_normal.logpdf(x, np.zeros(len(x)), np.ones(len(x))) + threshold))
        elif constraint_type == 'ball':
            opt.add_inequality_constraint(lambda x, grad: float(-np.linalg.norm(x) + threshold))
    if ftol_abs:
        opt.set_ftol_abs(ftol_abs)
    if maxeval:
        opt.set_maxeval(maxeval)
    if stopval:
        opt.set_stopval(stopval)
    sorted_score_indices = np.argsort(scores)

    local_optima, true_scores, optima_densities, affnist_losses = [], [], [], []
    if not threshold:
        threshold = 0 # Use any value, just so that density function can be used
    for i in range(1, t + 1):
        #print('i = {}'.format(i))
        print('Predicted score before local optimisation: {}'.format(scores[sorted_score_indices[-i]]), flush=True)
        initial_guess = np.array(valid_points.numpy()[sorted_score_indices[-i]])
        log_density = -exact_density_nlopt(initial_guess, [], means, variances, threshold) + threshold
        print('Log GMM density at initial guess: {}'.format(log_density), flush=True)
        local_optimum = opt.optimize(initial_guess)
        value = opt.last_optimum_value()
        local_optimum = torch.from_numpy(local_optimum).float().view(1, -1)
        print('Predicted optimised score: {}'.format(value), flush=True)
        with torch.no_grad():
            torch_image = model.decode(local_optimum)[0].float()
        image = torch_image.squeeze(0).squeeze(0).numpy()
        if objective == 'rotation' and image_grad_alg == 'PCA':
            binarised_image = (image > 0.5).astype(np.float) #np.random.binomial(1, image)
            true_score = execute_objective(binarised_image, objective, image_grad_alg)
        else:
            true_score = execute_objective(image, objective, image_grad_alg)
        print('True optimised score: {}'.format(true_score), flush=True)
        affnist_loss = get_loss(affnist_model, torch_image)
        print('Affnist loss: {}'.format(affnist_loss), flush=True)
        affnist_losses.append(affnist_loss)
        local_optima.append(local_optimum)
        true_scores.append(true_score)
        log_density = -exact_density_nlopt(initial_guess, [], means, variances, threshold) + threshold
        print('Log GMM density at optimum: {}'.format(log_density), flush=True)
        optima_densities.append(log_density)
        pil = smp.toimage(image)
        pil.save(os.path.join(save_dname, 'image_{}.jpg'.format(i)))
        print('----------', flush=True)

    local_optima = np.vstack(local_optima)
    true_scores = np.array(true_scores)
    affnist_losses = np.array(affnist_losses)
    np.savetxt(os.path.join(save_dname, 'local_optima.txt'), local_optima)
    np.savetxt(os.path.join(save_dname, 'true_optima_scores.txt'), true_scores)
    np.savetxt(os.path.join(save_dname, 'affnist_losses.txt'), affnist_losses)
    np.savetxt(os.path.join(save_dname, 'optima_densities.txt'), optima_densities)


parser = argparse.ArgumentParser()
parser.add_argument('--latent-size', type=int)
parser.add_argument('--dim-h', type=int)
parser.add_argument('--load-dname')
parser.add_argument('--gridpoints-path')
parser.add_argument('--log-densities-path')
parser.add_argument('--sd-path')
parser.add_argument('--affnist-sd-path')
parser.add_argument('--load-scores', action='store_true')
parser.add_argument('--threshold', type=float, default=None)
parser.add_argument('--maxeval', type=int, default=None)
parser.add_argument('--ftol-abs', type=float, default=0.001)
parser.add_argument('--stopval', type=float, default=None)
parser.add_argument('--constraint-type', default='latent_dist')
parser.add_argument('--batch-size', type=int, default=None)
parser.add_argument('--num-neighbours', type=int, default=1000)
parser.add_argument('--t', type=int, default=1)
parser.add_argument('--knn', action='store_true')
parser.add_argument('--gpu', action='store_true')
parser.add_argument('--image-grad-alg', choices=['least_squares', 'PCA'])
parser.add_argument('--objective', choices=['rotation', 'thickness', 'aspect_ratio'])
parser.add_argument('--k', type=int, default=None)
parser.add_argument('--vae-mode', choices=['VAE', 'IWAE'])
parser.add_argument('--vae-type', choices=['conv', 'conv2'])
parser.add_argument('--dim-intermediate', type=int, default=800)
parser.add_argument('--load-index', action='store_true')
parser.add_argument('--DEBUG', action='store_true')

if __name__ == '__main__':
    kwargs = vars(parser.parse_args())
    main(**kwargs)
